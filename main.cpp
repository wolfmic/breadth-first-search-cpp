#include <iostream>
#include "GridMap.h"

int main(int ac, char** av) {
	auto g = new GridMap(Vector2{ 10, 10 }, Vector2{ 1,1 }, Vector2{6,1});
	g->AddWall(Vector2{ 3, 0 });
	g->AddWall(Vector2{ 3, 1 });
	g->AddWall(Vector2{ 3, 2 });
	g->StartBreadth();
	g->Display();
	return 0;
}