#include "GridMap.h"

GridMap::GridMap(Vector2 s, Vector2 sp, Vector2 ep) : size(s), startposition(sp), endposition(ep) {
	AddStart(sp);
	AddEnd(ep);
}

GridMap::~GridMap() {
}

void GridMap::Display() {
	for (int j = 0; j < size.y; j++) {
		for (int i = 0; i < size.x; i++) {
			if (elements[j][i].type == EElementType::WALL) {
				std::cout << "# ";
			} else {
				std::cout << path[j][i] << " ";
			}
		}
		std::cout << std::endl;
	}
}

void GridMap::AddWall(Vector2 p) {
	elements[p.y][p.x].type = EElementType::WALL;
}

void GridMap::AddStart(Vector2 p) {
	elements[p.y][p.x].type = EElementType::START;
	elements[p.y][p.x].position = p;
	startposition = p;
}

void GridMap::AddEnd(Vector2 p) {
	elements[p.y][p.x].type = EElementType::END;
	elements[p.y][p.x].position = p;
	endposition = p;
}

void GridMap::StartBreadth() {
	if (startposition == Vector2{ -1,-1 } || endposition == Vector2{ -1, -1 }) {
		std::cout << "can't start" << std::endl;
	}

	tovisit.push(startposition);

	while (true) {
		auto position_to_visit = tovisit.front();
		tovisit.pop();

		if (elements[position_to_visit.y][position_to_visit.x].type == EElementType::END) {
			break;
		}

		auto neighbors = GetNeighbors(position_to_visit);

		for (auto neighbor : neighbors) {
			tovisit.push(neighbor);
		}

		if (tovisit.empty()) {
			std::cout << "empty" << std::endl;
			break;
		}
	}

	Vector2 tmp_position = Vector2{ endposition.x, endposition.y };
	while (tmp_position != startposition) {
		auto e = elements[tmp_position.y][tmp_position.x];
		if (e.direction == EDirection::UP) {
			tmp_position = tmp_position + alldir[1];
			path[tmp_position.y][tmp_position.x] = 'v';
		} else if (e.direction == EDirection::DOWN) {
			tmp_position = tmp_position + alldir[0];
			path[tmp_position.y][tmp_position.x] = '^';
		} else if (e.direction == EDirection::LEFT) {
			tmp_position = tmp_position + alldir[3];
			path[tmp_position.y][tmp_position.x] = '>';
		} else if (e.direction == EDirection::RIGHT) {
			tmp_position = tmp_position + alldir[2];
			path[tmp_position.y][tmp_position.x] = '<';
		} else {
			path[tmp_position.y][tmp_position.x] = ' ';
		}
	}
}

std::vector<Vector2> GridMap::GetNeighbors(Vector2 p) {
	auto r = std::vector<Vector2>();

	for (int i = 0; i < 4; i++) {
		auto tmp_position = p + alldir[i];
		if (InBound(*this, tmp_position) && elements[tmp_position.y][tmp_position.x].type != EElementType::VISITED && elements[tmp_position.y][tmp_position.x].type != EElementType::WALL && elements[tmp_position.y][tmp_position.x].type != EElementType::START) {
			if (elements[tmp_position.y][tmp_position.x].type != EElementType::END) {
				elements[tmp_position.y][tmp_position.x].type = EElementType::VISITED;
				elements[tmp_position.y][tmp_position.x].direction = static_cast<EDirection>(i);
			} else {
				elements[tmp_position.y][tmp_position.x].direction = static_cast<EDirection>(i);
			}
			r.push_back(tmp_position);
		}
	}
	return r;
}

bool GridMap::InBound(const GridMap& gm, const Vector2 p) {
	return p.x < gm.size.x && p.y < gm.size.y && p.x >= 0 && p.y >= 0;
}