#pragma once

#include <array>
#include <map>
#include <queue>
#include <iostream>

struct Vector2 {
	int x, y;

	inline bool operator==(Vector2 o) {
		return (this->x == o.x && this->y == o.y);
	}

	inline bool operator!=(Vector2 o) {
		return (this->x != o.x || this->y != o.y);
	}

	inline Vector2 operator+(const Vector2& o) {
		return Vector2{this->x + o.x, this->y + o.y};
	}

	inline const Vector2 operator-(const Vector2& o) const {
		return Vector2{ this->x - o.x, this->y - o.y };
	}

	inline Vector2 operator*(const Vector2& o) {
		return Vector2{ this->x * o.x, this->y - o.y};
	}

	inline Vector2 operator *(const int& i) {
		return Vector2{ this->x * i, this->y * i };
	}

	friend std::ostream& operator<<(std::ostream& os, const Vector2& o) {
		os << o.x << " - " << o.y;
		return os;
	}
};

enum EElementType {
	NONE,
	WALL,
	START,
	END,
	VISITED
};

enum EDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

struct SElement {
	Vector2			position;
	EElementType	type;
	EDirection		direction;
};

class GridMap {

private:
	std::map<int, std::map<int, SElement>>	elements;
	Vector2									size;
	Vector2									startposition;
	Vector2									endposition;
	std::queue<Vector2>						tovisit;
	std::map<int, std::map<int, char>>		path;


	const std::array<Vector2, 4> alldir {
		{
			Vector2{0,1},
			Vector2{0,-1},
			Vector2{1,0},
			Vector2{-1,0}
		}
	};

public:
	GridMap(Vector2 s, Vector2 sp, Vector2 ep);
	~GridMap();

public:
	static bool InBound(const GridMap& gm, const Vector2 p);
	void Display();
	void AddWall(Vector2 p);
	void AddStart(Vector2 p);
	void AddEnd(Vector2 p);
	void StartBreadth();
	std::vector<Vector2> GetNeighbors(Vector2 p);
};